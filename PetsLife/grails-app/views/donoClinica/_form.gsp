<%@ page import="petslife.DonoClinica" %>



<div class="fieldcontain ${hasErrors(bean: donoClinicaInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="donoClinica.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" maxlength="35" required="" value="${donoClinicaInstance?.nome}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: donoClinicaInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="donoClinica.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="email" required="" value="${donoClinicaInstance?.email}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: donoClinicaInstance, field: 'senha', 'error')} required">
	<label for="senha">
		<g:message code="donoClinica.senha.label" default="Senha" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="senha" maxlength="12" required="" value="${donoClinicaInstance?.senha}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: donoClinicaInstance, field: 'cpf', 'error')} required">
	<label for="cpf">
		<g:message code="donoClinica.cpf.label" default="Cpf" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="cpf" required="" value="${donoClinicaInstance?.cpf}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: donoClinicaInstance, field: 'rg', 'error')} required">
	<label for="rg">
		<g:message code="donoClinica.rg.label" default="Rg" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="rg" type="number" value="${donoClinicaInstance.rg}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: donoClinicaInstance, field: 'telefone', 'error')} required">
	<label for="telefone">
		<g:message code="donoClinica.telefone.label" default="Telefone" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="telefone" type="number" value="${donoClinicaInstance.telefone}" required=""/>

</div>

