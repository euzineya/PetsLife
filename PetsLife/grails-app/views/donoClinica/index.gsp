
<%@ page import="petslife.DonoClinica" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'donoClinica.label', default: 'DonoClinica')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-donoClinica" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-donoClinica" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="nome" title="${message(code: 'donoClinica.nome.label', default: 'Nome')}" />
					
						<g:sortableColumn property="email" title="${message(code: 'donoClinica.email.label', default: 'Email')}" />
					
						<g:sortableColumn property="senha" title="${message(code: 'donoClinica.senha.label', default: 'Senha')}" />
					
						<g:sortableColumn property="cpf" title="${message(code: 'donoClinica.cpf.label', default: 'Cpf')}" />
					
						<g:sortableColumn property="rg" title="${message(code: 'donoClinica.rg.label', default: 'Rg')}" />
					
						<g:sortableColumn property="telefone" title="${message(code: 'donoClinica.telefone.label', default: 'Telefone')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${donoClinicaInstanceList}" status="i" var="donoClinicaInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${donoClinicaInstance.id}">${fieldValue(bean: donoClinicaInstance, field: "nome")}</g:link></td>
					
						<td>${fieldValue(bean: donoClinicaInstance, field: "email")}</td>
					
						<td>${fieldValue(bean: donoClinicaInstance, field: "senha")}</td>
					
						<td>${fieldValue(bean: donoClinicaInstance, field: "cpf")}</td>
					
						<td>${fieldValue(bean: donoClinicaInstance, field: "rg")}</td>
					
						<td>${fieldValue(bean: donoClinicaInstance, field: "telefone")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${donoClinicaInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
