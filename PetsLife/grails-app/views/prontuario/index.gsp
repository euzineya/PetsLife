
<%@ page import="petslife.Prontuario" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'prontuario.label', default: 'Prontuario')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-prontuario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-prontuario" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="vacina" title="${message(code: 'prontuario.vacina.label', default: 'Vacina')}" />
					
						<g:sortableColumn property="doenca" title="${message(code: 'prontuario.doenca.label', default: 'Doenca')}" />
					
						<th><g:message code="prontuario.medico.label" default="Medico" /></th>
					
						<th><g:message code="prontuario.pet.label" default="Pet" /></th>
					
						<g:sortableColumn property="sintoma" title="${message(code: 'prontuario.sintoma.label', default: 'Sintoma')}" />
					
						<g:sortableColumn property="tratamento" title="${message(code: 'prontuario.tratamento.label', default: 'Tratamento')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${prontuarioInstanceList}" status="i" var="prontuarioInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${prontuarioInstance.id}">${fieldValue(bean: prontuarioInstance, field: "vacina")}</g:link></td>
					
						<td>${fieldValue(bean: prontuarioInstance, field: "doenca")}</td>
					
						<td>${fieldValue(bean: prontuarioInstance, field: "medico")}</td>
					
						<td>${fieldValue(bean: prontuarioInstance, field: "pet")}</td>
					
						<td>${fieldValue(bean: prontuarioInstance, field: "sintoma")}</td>
					
						<td>${fieldValue(bean: prontuarioInstance, field: "tratamento")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${prontuarioInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
