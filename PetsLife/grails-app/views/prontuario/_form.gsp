<%@ page import="petslife.Prontuario" %>



<div class="fieldcontain ${hasErrors(bean: prontuarioInstance, field: 'vacina', 'error')} required">
	<label for="vacina">
		<g:message code="prontuario.vacina.label" default="Vacina" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="vacina" from="${prontuarioInstance.constraints.vacina.inList}" required="" value="${prontuarioInstance?.vacina}" valueMessagePrefix="prontuario.vacina"/>

</div>

<div class="fieldcontain ${hasErrors(bean: prontuarioInstance, field: 'doenca', 'error')} required">
	<label for="doenca">
		<g:message code="prontuario.doenca.label" default="Doenca" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="doenca" required="" value="${prontuarioInstance?.doenca}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: prontuarioInstance, field: 'medico', 'error')} required">
	<label for="medico">
		<g:message code="prontuario.medico.label" default="Medico" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="medico" name="medico.id" from="${petslife.Medico.list()}" optionKey="id" required="" value="${prontuarioInstance?.medico?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: prontuarioInstance, field: 'pet', 'error')} required">
	<label for="pet">
		<g:message code="prontuario.pet.label" default="Pet" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="pet" name="pet.id" from="${petslife.Pet.list()}" optionKey="id" required="" value="${prontuarioInstance?.pet?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: prontuarioInstance, field: 'sintoma', 'error')} required">
	<label for="sintoma">
		<g:message code="prontuario.sintoma.label" default="Sintoma" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="sintoma" required="" value="${prontuarioInstance?.sintoma}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: prontuarioInstance, field: 'tratamento', 'error')} required">
	<label for="tratamento">
		<g:message code="prontuario.tratamento.label" default="Tratamento" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="tratamento" required="" value="${prontuarioInstance?.tratamento}"/>

</div>

