
<%@ page import="petslife.Prontuario" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'prontuario.label', default: 'Prontuario')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-prontuario" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-prontuario" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list prontuario">
			
				<g:if test="${prontuarioInstance?.vacina}">
				<li class="fieldcontain">
					<span id="vacina-label" class="property-label"><g:message code="prontuario.vacina.label" default="Vacina" /></span>
					
						<span class="property-value" aria-labelledby="vacina-label"><g:fieldValue bean="${prontuarioInstance}" field="vacina"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${prontuarioInstance?.doenca}">
				<li class="fieldcontain">
					<span id="doenca-label" class="property-label"><g:message code="prontuario.doenca.label" default="Doenca" /></span>
					
						<span class="property-value" aria-labelledby="doenca-label"><g:fieldValue bean="${prontuarioInstance}" field="doenca"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${prontuarioInstance?.medico}">
				<li class="fieldcontain">
					<span id="medico-label" class="property-label"><g:message code="prontuario.medico.label" default="Medico" /></span>
					
						<span class="property-value" aria-labelledby="medico-label"><g:link controller="medico" action="show" id="${prontuarioInstance?.medico?.id}">${prontuarioInstance?.medico?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${prontuarioInstance?.pet}">
				<li class="fieldcontain">
					<span id="pet-label" class="property-label"><g:message code="prontuario.pet.label" default="Pet" /></span>
					
						<span class="property-value" aria-labelledby="pet-label"><g:link controller="pet" action="show" id="${prontuarioInstance?.pet?.id}">${prontuarioInstance?.pet?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${prontuarioInstance?.sintoma}">
				<li class="fieldcontain">
					<span id="sintoma-label" class="property-label"><g:message code="prontuario.sintoma.label" default="Sintoma" /></span>
					
						<span class="property-value" aria-labelledby="sintoma-label"><g:fieldValue bean="${prontuarioInstance}" field="sintoma"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${prontuarioInstance?.tratamento}">
				<li class="fieldcontain">
					<span id="tratamento-label" class="property-label"><g:message code="prontuario.tratamento.label" default="Tratamento" /></span>
					
						<span class="property-value" aria-labelledby="tratamento-label"><g:fieldValue bean="${prontuarioInstance}" field="tratamento"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:prontuarioInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${prontuarioInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
