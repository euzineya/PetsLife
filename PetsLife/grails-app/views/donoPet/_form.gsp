<%@ page import="petslife.DonoPet" %>



<div class="fieldcontain ${hasErrors(bean: donoPetInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="donoPet.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" required="" value="${donoPetInstance?.nome}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: donoPetInstance, field: 'cpf', 'error')} required">
	<label for="cpf">
		<g:message code="donoPet.cpf.label" default="Cpf" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="cpf" required="" value="${donoPetInstance?.cpf}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: donoPetInstance, field: 'telefone', 'error')} required">
	<label for="telefone">
		<g:message code="donoPet.telefone.label" default="Telefone" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="telefone" type="number" value="${donoPetInstance.telefone}" required=""/>

</div>

