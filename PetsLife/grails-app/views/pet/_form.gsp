<%@ page import="petslife.Pet" %>



<div class="fieldcontain ${hasErrors(bean: petInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="pet.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" required="" value="${petInstance?.nome}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: petInstance, field: 'especie', 'error')} required">
	<label for="especie">
		<g:message code="pet.especie.label" default="Especie" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="especie" from="${petInstance.constraints.especie.inList}" required="" value="${petInstance?.especie}" valueMessagePrefix="pet.especie"/>

</div>

<div class="fieldcontain ${hasErrors(bean: petInstance, field: 'donopet', 'error')} required">
	<label for="donopet">
		<g:message code="pet.donopet.label" default="Donopet" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="donopet" name="donopet.id" from="${petslife.DonoPet.list()}" optionKey="id" required="" value="${petInstance?.donopet?.id}" class="many-to-one"/>

</div>

