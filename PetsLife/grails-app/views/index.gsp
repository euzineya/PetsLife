<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>PetsLife</title>
		<style type="text/css" media="screen">
                    #po1{
                          top: 380px;  
                          position: absolute;
                          left: 610px;
                    }
                     #po2{
                          position: absolute;
                          top: 440px;
                          left: 610px;
                    }
                    #po3{
                          position: absolute;
                          top: 500px;
                          left: 610px;
                    }
                    #po4{
                          position: absolute;
                          top: 560px;
                          left: 610px;
                    }
                    #po5{
                          position: absolute;
                          top: 620px;
                          left: 610px;
                    }
         
                    #po6{
                          position: absolute;
                          top: 680px;
                          left: 610px;
                    }
                    
                    #po7{
                          position: absolute;
                          top: 740px;
                          left: 610px;
                    }
                    #po8{
                          position: absolute;
                          top: 800px;
                          left: 610px;
                    }
		</style>
	</head>
	<body>
		<div id="page-body" role="main">

			<div id="controller-list" role="navigation">
				<center><h2>Sistema de Gerenciamento de Clinicas Veterinarias</h2></center>
				<ul>
                                    <div id="po1"><g:link button class="btn btn-laranja" controller="DonoClinica" action="index"><g:message code="Gerenciar Dono da Clinica" args="[entityName]" /></g:link></div> 
                                    <div id="po2"><g:link button class="btn btn-laranja" controller="DonoPet" action="index"><g:message code="Gerenciar Dono do Pet" args="[entityName]" /></g:link></div> 
                                    <div id="po3"><g:link button class="btn btn-laranja" controller="Pet" action="index"><g:message code="Gerenciar Pet" args="[entityName]" /></g:link></div> 
                                    <div id="po4"><g:link button class="btn btn-laranja" controller="Prontuario" action="index"><g:message code="Prontuario" args="[entityName]" /></g:link></div> 
                                    <div id="po5"><g:link button class="btn btn-laranja" controller="Medico" action="index"><g:message code="Gerenciar Médico" args="[entityName]" /></g:link></div> 
                                    <div id="po6"><g:link button class="btn btn-laranja" controller="Lista" action="index"><g:message code="Realizar Busca do Dono do Pet" args="[entityName]" /></g:link></div> 
                                    <div id="po7"><g:link button class="btn btn-laranja" controller="DonoClinica" action="index"><g:message code="Gerenciar Dono da Clinica" args="[entityName]" /></g:link></div> 
                                    <div id="po8"><g:link button class="btn btn-laranja" controller="logout" action="index"><g:message code="Sair" args="[entityName]" /></g:link></div>                                                                                                                                                                           
                                </ul> 
			</div>
		</div>
	</body>
</html>
