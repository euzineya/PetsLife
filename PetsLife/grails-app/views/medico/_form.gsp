<%@ page import="petslife.Medico" %>



<div class="fieldcontain ${hasErrors(bean: medicoInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="medico.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" maxlength="35" required="" value="${medicoInstance?.nome}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: medicoInstance, field: 'cpf', 'error')} required">
	<label for="cpf">
		<g:message code="medico.cpf.label" default="Cpf" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="cpf" required="" value="${medicoInstance?.cpf}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: medicoInstance, field: 'rg', 'error')} required">
	<label for="rg">
		<g:message code="medico.rg.label" default="Rg" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="rg" type="number" value="${medicoInstance.rg}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: medicoInstance, field: 'telefone', 'error')} required">
	<label for="telefone">
		<g:message code="medico.telefone.label" default="Telefone" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="telefone" type="number" value="${medicoInstance.telefone}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: medicoInstance, field: 'diploma', 'error')} required">
	<label for="diploma">
		<g:message code="medico.diploma.label" default="Diploma" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="diploma" required="" value="${medicoInstance?.diploma}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: medicoInstance, field: 'idade', 'error')} required">
	<label for="idade">
		<g:message code="medico.idade.label" default="Idade" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="idade" type="number" value="${medicoInstance.idade}" required=""/>

</div>

