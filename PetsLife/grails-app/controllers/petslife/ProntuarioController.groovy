package petslife



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import org.springframework.security.access.annotation.Secured
@Secured('isFullyAuthenticated()')

@Transactional(readOnly = true)
class ProntuarioController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Prontuario.list(params), model:[prontuarioInstanceCount: Prontuario.count()]
    }

    def show(Prontuario prontuarioInstance) {
        respond prontuarioInstance
    }

    def create() {
        respond new Prontuario(params)
    }

    @Transactional
    def save(Prontuario prontuarioInstance) {
        if (prontuarioInstance == null) {
            notFound()
            return
        }

        if (prontuarioInstance.hasErrors()) {
            respond prontuarioInstance.errors, view:'create'
            return
        }

        prontuarioInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'prontuario.label', default: 'Prontuario'), prontuarioInstance.id])
                redirect prontuarioInstance
            }
            '*' { respond prontuarioInstance, [status: CREATED] }
        }
    }

    def edit(Prontuario prontuarioInstance) {
        respond prontuarioInstance
    }

    @Transactional
    def update(Prontuario prontuarioInstance) {
        if (prontuarioInstance == null) {
            notFound()
            return
        }

        if (prontuarioInstance.hasErrors()) {
            respond prontuarioInstance.errors, view:'edit'
            return
        }

        prontuarioInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Prontuario.label', default: 'Prontuario'), prontuarioInstance.id])
                redirect prontuarioInstance
            }
            '*'{ respond prontuarioInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Prontuario prontuarioInstance) {

        if (prontuarioInstance == null) {
            notFound()
            return
        }

        prontuarioInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Prontuario.label', default: 'Prontuario'), prontuarioInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'prontuario.label', default: 'Prontuario'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
