package petslife



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import org.springframework.security.access.annotation.Secured
@Secured('isFullyAuthenticated()')

@Transactional(readOnly = true)
class PetController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Pet.list(params), model:[petInstanceCount: Pet.count()]
    }

    def show(Pet petInstance) {
        respond petInstance
    }

    def create() {
        respond new Pet(params)
    }

    @Transactional
    def save(Pet petInstance) {
        if (petInstance == null) {
            notFound()
            return
        }

        if (petInstance.hasErrors()) {
            respond petInstance.errors, view:'create'
            return
        }

        petInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'pet.label', default: 'Pet'), petInstance.id])
                redirect petInstance
            }
            '*' { respond petInstance, [status: CREATED] }
        }
    }

    def edit(Pet petInstance) {
        respond petInstance
    }

    @Transactional
    def update(Pet petInstance) {
        if (petInstance == null) {
            notFound()
            return
        }

        if (petInstance.hasErrors()) {
            respond petInstance.errors, view:'edit'
            return
        }

        petInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Pet.label', default: 'Pet'), petInstance.id])
                redirect petInstance
            }
            '*'{ respond petInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Pet petInstance) {

        if (petInstance == null) {
            notFound()
            return
        }

        petInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Pet.label', default: 'Pet'), petInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'pet.label', default: 'Pet'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
