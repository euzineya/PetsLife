package petslife



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import org.springframework.security.access.annotation.Secured
@Secured('isFullyAuthenticated()')

@Transactional(readOnly = true)
class DonoPetController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond DonoPet.list(params), model:[donoPetInstanceCount: DonoPet.count()]
    }

    def show(DonoPet donoPetInstance) {
        respond donoPetInstance
    }

    def create() {
        respond new DonoPet(params)
    }

    @Transactional
    def save(DonoPet donoPetInstance) {
        if (donoPetInstance == null) {
            notFound()
            return
        }

        if (donoPetInstance.hasErrors()) {
            respond donoPetInstance.errors, view:'create'
            return
        }

        donoPetInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'donoPet.label', default: 'DonoPet'), donoPetInstance.id])
                redirect donoPetInstance
            }
            '*' { respond donoPetInstance, [status: CREATED] }
        }
    }

    def edit(DonoPet donoPetInstance) {
        respond donoPetInstance
    }

    @Transactional
    def update(DonoPet donoPetInstance) {
        if (donoPetInstance == null) {
            notFound()
            return
        }

        if (donoPetInstance.hasErrors()) {
            respond donoPetInstance.errors, view:'edit'
            return
        }

        donoPetInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'DonoPet.label', default: 'DonoPet'), donoPetInstance.id])
                redirect donoPetInstance
            }
            '*'{ respond donoPetInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(DonoPet donoPetInstance) {

        if (donoPetInstance == null) {
            notFound()
            return
        }

        donoPetInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'DonoPet.label', default: 'DonoPet'), donoPetInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'donoPet.label', default: 'DonoPet'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
