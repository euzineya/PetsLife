package petslife



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import org.springframework.security.access.annotation.Secured
@Secured('isFullyAuthenticated()')

@Transactional(readOnly = true)
class DonoClinicaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond DonoClinica.list(params), model:[donoClinicaInstanceCount: DonoClinica.count()]
    }

    def show(DonoClinica donoClinicaInstance) {
        respond donoClinicaInstance
    }

    def create() {
        respond new DonoClinica(params)
    }

    @Transactional
    def save(DonoClinica donoClinicaInstance) {
        if (donoClinicaInstance == null) {
            notFound()
            return
        }

        if (donoClinicaInstance.hasErrors()) {
            respond donoClinicaInstance.errors, view:'create'
            return
        }

        donoClinicaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'donoClinica.label', default: 'DonoClinica'), donoClinicaInstance.id])
                redirect donoClinicaInstance
            }
            '*' { respond donoClinicaInstance, [status: CREATED] }
        }
    }

    def edit(DonoClinica donoClinicaInstance) {
        respond donoClinicaInstance
    }

    @Transactional
    def update(DonoClinica donoClinicaInstance) {
        if (donoClinicaInstance == null) {
            notFound()
            return
        }

        if (donoClinicaInstance.hasErrors()) {
            respond donoClinicaInstance.errors, view:'edit'
            return
        }

        donoClinicaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'DonoClinica.label', default: 'DonoClinica'), donoClinicaInstance.id])
                redirect donoClinicaInstance
            }
            '*'{ respond donoClinicaInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(DonoClinica donoClinicaInstance) {

        if (donoClinicaInstance == null) {
            notFound()
            return
        }

        donoClinicaInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'DonoClinica.label', default: 'DonoClinica'), donoClinicaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'donoClinica.label', default: 'DonoClinica'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
