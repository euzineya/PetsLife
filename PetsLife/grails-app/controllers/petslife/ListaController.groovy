package petslife

import org.springframework.security.access.annotation.Secured
@Secured('isFullyAuthenticated()')

class ListaController {

    def index() { 
      println("passou aqui - index Lista")
        
        def consulta = DonoPet.findAll()
        
        def nome = DonoPet.findByNome('Rufino')
        println "B = " + nome
        
        def consultaF = DonoPet.executeQuery("Select distinct a from DonoPet a")        
        print consultaF
        
        println("consulta = " +consulta.nome+ "=" +consulta.cpf)
        render (view:'index', model:[consultaNaView:consulta])
    }
    
    def resultadoAjax = {
        println " madara = "+params.nome
        def busca = DonoPet.findByNomeLike("%${params.nome}%")
    
        if (busca){
            render(template:'resultado', model:[resultado:busca])
        }else{
            flash.message = "Elemento não encontrado!"
            render(template:'resultado', 
            model:[resultado:busca],
            method:'GET')
        }
                
    }
}
