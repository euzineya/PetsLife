package petslife

class DonoPet {
    String nome
    String cpf
    int telefone
    
    static constraints = {  
        nome nome:true, unique:true
        cpf cpf:true, unique:true
        telefone telefone:true, unique:true
    }
    
    String toString(){
        nome
    }
}
