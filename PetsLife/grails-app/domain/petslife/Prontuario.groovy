package petslife

class Prontuario {

    String tratamento
    String vacina
    String sintoma
    String doenca
  
    //Medico medico
    Pet pet
    Medico medico
     
    static constraints = {
        vacina (inList:["Vacina contra Gripe", "Vacina contra Raiva", "Outros"])
    }
}
