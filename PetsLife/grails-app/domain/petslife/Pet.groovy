package petslife

class Pet {

    String nome
    String especie

    DonoPet donopet 
    
    static constraints = {
         nome nullabel:false, blank: false
         especie (inList:["Cachorro", "Gato", "Outros"])
    }
    
    String toString(){
        nome
    }
}
