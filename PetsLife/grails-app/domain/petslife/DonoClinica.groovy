package petslife

class DonoClinica {
    String nome
    String cpf
    String senha
    String email
    int telefone
    int rg
    
    static constraints = {
        nome nullabel:false, blank: false, maxSize:35
        email email:true, unique:true
        senha size: 8..12
    }
   
    String toString(){
        nome
    }
    
}
