package petslife

class Medico {
    String nome
    String cpf
    String diploma
    int idade
    int telefone
    int rg
    
    static constraints = {
        nome nullabel:false, blank: false, maxSize:35
        cpf cpf:true, unique:true        
        rg rg:true, unique:true
        telefone telefone:true, unique:true
    }
    
    String toString(){
        nome
    }
}
